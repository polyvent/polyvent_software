#include <SPI.h>

const int ports[12] = {2, 4, 5, 13, 14, 15, 16, 17, 21, 22, 25, 26}; // ss pins of each spi port


void setup() {
  Serial.begin(115200);
  SPI.begin();
  SPI.beginTransaction(SPISettings(10000, MSBFIRST, SPI_MODE0));
  
  Serial.println("starting");
  
  for(int portnum = 0; portnum < 12; portnum++){
    pinMode(ports[portnum], OUTPUT);
    digitalWrite(ports[portnum], HIGH); //turned off that ss pin
  }

  for(int portnum = 0; portnum < 12; portnum++){
    digitalWrite(ports[portnum], LOW);
    char answer_tag = SPI.transfer('$');
    if(answer_tag == 'A'){
      Serial.print("motor board was found on port ");
      Serial.println(portnum);
    }
    else if(answer_tag == 'B'){
      Serial.print("valves board was found on port ");
      Serial.println(portnum);
    digitalWrite(ports[portnum], HIGH);
    }
    else{
      Serial.print("nothing or a pressure sensor on port ");
      Serial.println(portnum);
    }
  } 
}
void loop() {
}
