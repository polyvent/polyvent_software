#include "Arduino.h"
#include "SPI.h"
#include "MultiStepper.h"
#include "AccelStepper.h"

byte received_signal_raw_bytes[12]; //raw bytes unloaded from spdr
uint16_t received_signal_reconstructed[6] = {0, 0, 0, 0, 0, 0}; //data coming out of the spi port reformated into int16's

volatile byte indx = 0;
volatile boolean process;
byte homestatus1, homestatus2;
long stepper1PosHome, stepper2PosHome;

//permanant pin mapping order: step, dir, enable
const int pins_for_stepper_1[3] = {4, 5, 6};
const int pins_for_stepper_2[3] = {7, 8, 9};
const int switch_pins[2] = {2, 3};
volatile byte switch_states[2];

////Declaration of steppers//////////
AccelStepper stepper1(1, 4, 5); 
AccelStepper stepper2(1, 7, 8); 

void setup(void)
{
  Serial.begin(500000);

  pinMode(MISO, OUTPUT); // have to send on master in so it set as output

  SPCR |= _BV(SPE);      // turn on SPI in slave mode
  SPI.attachInterrupt(); // turn on interrupt
  indx = 0;              // received_signal_raw_bytes scal
  process = false;

  pinMode(switch_pins, INPUT);
  pinMode(pins_for_stepper_1, OUTPUT);
  pinMode(pins_for_stepper_2, OUTPUT);

  digitalWrite(pins_for_stepper_1[2], HIGH);
  digitalWrite(pins_for_stepper_2[2], HIGH);

// These are homing switch interrupts that we are not using now...
//  attachInterrupt(digitalPinToInterrupt(switch_pins[0]), switch_state_stepper_1, CHANGE);
//  attachInterrupt(digitalPinToInterrupt(switch_pins[1]), switch_state_stepper_2, CHANGE);
}

void loop(void)
{
  if (process) {
    for(int x = 0; x<6; x++){
        received_signal_reconstructed[x] = readuint16_t((x * 2), received_signal_raw_bytes);
        Serial.print("i : ");
        Serial.print(x);
        Serial.print(" : ");
        Serial.println(received_signal_reconstructed[x]);
    }
    stepper1.setAcceleration((float) received_signal_reconstructed[0]);
    stepper2.setAcceleration((float) received_signal_reconstructed[1]);
    stepper1.setMaxSpeed((float) received_signal_reconstructed[2]);
    stepper2.setMaxSpeed((float) received_signal_reconstructed[3]);
    stepper1.moveTo(received_signal_reconstructed[4]);
    stepper2.moveTo(received_signal_reconstructed[5]);

    indx = 0;
    process = false;
  }

// Call these as often as possible.
  stepper1.run();
  stepper2.run();
}

//void switch_state_stepper_1() {
//  switch_states[0] = digitalRead(switch_pins[0]);
//}
//void switch_state_stepper_2() {
//  switch_states[1] = digitalRead(switch_pins[1]);
//}

ISR(SPI_STC_vect) // SPI interrupt routine
{
  byte c = SPDR; // read byte from SPI Data Register
  if (indx < sizeof received_signal_raw_bytes) {
    received_signal_raw_bytes[indx] = c; // save data in the next index in the array received_signal_raw_bytes
    indx++;
  }
  if (indx >= sizeof received_signal_raw_bytes) {
    process = true;
  }
}
