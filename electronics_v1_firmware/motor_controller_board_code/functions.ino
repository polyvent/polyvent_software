void homestepper1()
{
  while (switch_states[0] == 0) {
    stepper1.runSpeed();
    SPDR = 0;
  }
  SPDR = 1;
  bitWrite(received_signal_raw_bytes[12], 5, false);
  stepper1.setCurrentPosition(0);
  //  stepper1.setSpeed(received_signal_reconstructed[2]);
  //  stepper1.setAcceleration(received_signal_reconstructed[0]);
  // stepper1.setSpeed(500);
  // stepper1.setAcceleration(200);
  // stepper1.moveTo(5000);
}

void homestepper2()
{
  while (switch_states[1] == 0) {
    stepper2.runSpeed();
    SPDR = 0;
  }
  SPDR = 1;
  bitWrite(received_signal_raw_bytes[12], 4, false);
  stepper2.setCurrentPosition(0);
  //  stepper2.setSpeed(received_signal_reconstructed[3]);
  //  stepper2.setAcceleration(received_signal_reconstructed[1]);
  //  stepper2.setSpeed(500);
  // stepper2.setAcceleration(200);

}
uint16_t readuint16_t(int loc_in_buffer, byte *buff)
{
  int n = loc_in_buffer;
  uint16_t v;
  *(((byte *)&v)) = buff[n];
  *(((byte *)&v) + 1) = buff[n + 1];
  return v;
}
